defmodule Blog.Repo.Migrations.CreateTimer do
  use Ecto.Migration

  def change do
    create table(:timers) do
      add :title, :string
      add :start_time, :datetime
      add :end_time, :datetime
      add :owner, :string
      add :billable, :boolean, default: false

      timestamps
    end

  end
end
