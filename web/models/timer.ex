defmodule Blog.Timer do
  use Blog.Web, :model

  schema "timers" do
    field :title, :string
    field :start_time, Ecto.DateTime
    field :end_time, Ecto.DateTime
    field :owner, :string
    field :billable, :boolean, default: false

    timestamps
  end

  @required_fields ~w(title start_time end_time owner billable)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
