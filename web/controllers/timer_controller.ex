defmodule Blog.TimerController do
  use Blog.Web, :controller

  alias Blog.Timer

  plug :scrub_params, "timer" when action in [:create, :update]

  def index(conn, _params) do
    timers = Repo.all(Timer)
    render(conn, "index.html", timers: timers)
  end

  def new(conn, _params) do
    changeset = Timer.changeset(%Timer{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"timer" => timer_params}) do
    changeset = Timer.changeset(%Timer{}, timer_params)

    case Repo.insert(changeset) do
      {:ok, _timer} ->
        conn
        |> put_flash(:info, "Timer created successfully.")
        |> redirect(to: timer_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    timer = Repo.get!(Timer, id)
    render(conn, "show.html", timer: timer)
  end

  def edit(conn, %{"id" => id}) do
    timer = Repo.get!(Timer, id)
    changeset = Timer.changeset(timer)
    render(conn, "edit.html", timer: timer, changeset: changeset)
  end

  def update(conn, %{"id" => id, "timer" => timer_params}) do
    timer = Repo.get!(Timer, id)
    changeset = Timer.changeset(timer, timer_params)

    case Repo.update(changeset) do
      {:ok, timer} ->
        conn
        |> put_flash(:info, "Timer updated successfully.")
        |> redirect(to: timer_path(conn, :show, timer))
      {:error, changeset} ->
        render(conn, "edit.html", timer: timer, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    timer = Repo.get!(Timer, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(timer)

    conn
    |> put_flash(:info, "Timer deleted successfully.")
    |> redirect(to: timer_path(conn, :index))
  end
end
