defmodule Blog.TimerTest do
  use Blog.ModelCase

  alias Blog.Timer

  @valid_attrs %{billable: true, end_time: "2010-04-17 14:00:00", owner: "some content", start_time: "2010-04-17 14:00:00", title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Timer.changeset(%Timer{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Timer.changeset(%Timer{}, @invalid_attrs)
    refute changeset.valid?
  end
end
